﻿using System.Reflection;
using System.Web;
using FastReflectionLib;

namespace QJY.API
{
    public class YCGLManage : IWsService
    {
        public void ProcessRequest(HttpContext context, ref Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            MethodInfo methodInfo = typeof(YCGLManage).GetMethod(msg.Action.ToUpper());
            YCGLManage model = new YCGLManage();
            methodInfo.FastInvoke(model, new object[] { context, msg, P1, P2, UserInfo });
        }
        #region 车辆管理

      

        #region 获取所有车辆
        /// <summary>
        /// 获取所有车辆
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void GETALLCLLIST(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            string strSql = string.Format("SELECT car.* from SZHL_YCGL_CAR car Where car.ComId='10334' and car.Status='可用' ", UserInfo.User.ComId);
            msg.Result = new JH_Auth_UserB().GetDTByCommand(strSql);
        }
        #endregion



        #region 用车管理日历视图
        /// <summary>
        /// 用车管理日历视图
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void GETYCGLVIEW(HttpContext context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            string strSql = string.Format("SELECT  ycgl.ID,ycgl.intProcessStanceid,car.CarBrand+'-'+car.CarType+'-'+car.CarNum+'  '+CONVERT(VARCHAR(5),ycgl.StartTime,8)+'~'+CONVERT(VARCHAR(5),ycgl.EndTime,8) title,ycgl.StartTime start, ycgl.EndTime[end]  from SZHL_YCGL ycgl left outer join SZHL_YCGL_CAR car on ycgl.CarID = car.ID   where 1=1 ");
            if (P1 != "0")
            {
                strSql += string.Format(" and ycgl.CarID={0} ", P1);
            }
            msg.Result = new JH_Auth_UserB().GetDTByCommand(strSql);
        }
        #endregion


      
      

     
        #endregion
    }
}